<div class="card-body text-center mt-3 mb-3">
    <i class="fa fa-4x fa-{{ $icon }} mb-3 text-primary"></i>
    <div class="h4">
        {{ $text }}
    </div>
</div>
